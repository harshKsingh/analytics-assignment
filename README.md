# Analytics Assignment


This is the solution of analytics competition organized by INNVOCCER Summer Internship Program.
This repository contains Deduplication dataset in which we were asked to identify the unique patients based on their distinguishable features like age, gender and last name...etc. Note that the implementation is written in python.

This problem is primarley captures the concepts on unsupervised learning. So, I have used unsupervised techniques like k-means
k-medians clustering and some linear and non-linear dimensionlity reduction methods.


# Requirements

Basic data manupulating libarary : Pandas

Scientific Computing libarary : Numpy 

3D - Visulization library: plotly, Matplotlib

ML Library : Scikit-learn

# Dataset Description

In the given dataset, we have exactley 104 sample observation of patients in which each patient featurize by 4 predictors; Date of birth, gender, first name and last name.
